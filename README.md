# BD pdf2cbz

Simple service to convert pdf comics to cbz comics.

## Docker compose example

```yaml
version: '3'
services:
  bd_pdf2cbz:
    build: .
    restart: "no"
    volumes:
      - ./path/with/pdf/to/convert:/BDs:rw
    ports:
      - "5000:5000"
    environment:
      - BD_PDF2CBZ_BD_DIR=/BDs
```
