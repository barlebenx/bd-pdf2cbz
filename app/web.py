import re
import sys
import os
import shutil
from flask import Flask, render_template, jsonify, request
from BD_pdf2cbz import create_images, create_zip

if not 'BD_PDF2CBZ_BD_DIR' in os.environ:
    print('BD_PDF2CBZ_BD_DIR not defined')
    sys.exit(1)

bd_dir = os.environ['BD_PDF2CBZ_BD_DIR']

if not os.path.isdir(bd_dir):
    print('dir {} not found'.format(bd_dir))
    sys.exit(1)

if 'BD_PDF2CBZ_TMP_DIR' in os.environ:
    tmp_dir = os.environ['BD_PDF2CBZ_TMP_DIR']
else:
    tmp_dir = "/tmp/bd_pdf2cbz"
os.makedirs(tmp_dir, exist_ok=True)

def list_pdf_in_bd_dir():
    res = []
    for dirname, dirs, files in os.walk(bd_dir):
        for file in files:
            if os.path.splitext(file)[1] in ['.pdf','.PDF']:
                res.append(file)
    return res

app = Flask(__name__)

@app.route('/')
def index():
    pdf_list = list_pdf_in_bd_dir()
    return render_template("index.html", 
            pdf_list=pdf_list,
            bd_dir = bd_dir
        )

def assert_arg(arg_key, args = {}, expected_type=None, available_values_list=None, regex_value=None, int_range=None, int_min_value=None):
    if not arg_key in args:
        raise Exception('{} not found'.format(arg_key))
    if expected_type != None:
        if not type(args[arg_key]) == expected_type:
            raise Exception('{} invalid type'.format(arg_key))
    if available_values_list != None:
        if not args[arg_key] in available_values_list:
            raise Exception('{} invalid value'.format(arg_key))
    if regex_value != None:
        regex = re.compile(regex_value)
        if re.match(regex, args[arg_key]) == None:
            raise Exception('{} invalid value'.format(arg_key))
    if int_range != None:
        if int_range[0] > args[arg_key] or int_range[1] < args[arg_key]:
            raise Exception('{} invalid value'.format(arg_key))
    if int_min_value != None:
        if (not type(args[arg_key]) == int) or (args[arg_key] < int_min_value):
            raise Exception('{} invalid type'.format(arg_key))

@app.route('/convert', methods = ['POST'])
def convert():
    if not request.is_json:
        return '', 400

    try:
        content = request.get_json()
        assert_arg('dpi', content, expected_type=int, int_min_value=0)
        assert_arg('jpeg_quality', content, expected_type=int, int_range=(0,100))
        assert_arg('target', content, expected_type=str)
        
        target = content["target"]
        dpi = content["dpi"]
        jpeg_quality = content["jpeg_quality"]
        bd_basename = os.path.splitext(target)[0]
        work_dir = os.path.join(tmp_dir, bd_basename)

        if not os.path.isfile(os.path.join(bd_dir, target)):
            raise Exception('target not found')
    except Exception:
        return '', 400

    if os.path.isdir(work_dir):
        # delete old work_dir if already exists
        shutil.rmtree(work_dir)
    os.makedirs(work_dir, exist_ok=False)

    create_images(
        bd_file = os.path.join(bd_dir, target),
        images_dir = work_dir,
        dpi=dpi,
        jpeg_quality=jpeg_quality
    )
    create_zip(
        images_dir = work_dir,
        zip_path = os.path.join(bd_dir, bd_basename + '.cbz')
    )

    if os.path.isdir(os.path.join(tmp_dir, bd_basename)):
        # Clean up work_dir folder
        shutil.rmtree(work_dir)

    output = dict(
        status = "success",
        name = bd_basename + '.cbz'
    )
    return jsonify(output)

if __name__ == '__main__':
    app.run()
