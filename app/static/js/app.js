$(document).ready(function(){
    $("#submit_button").click(function(){
        $(this).hide()
        $('#result').html('<div class="alert alert-info">Convertion en cour...</div>')

        $.ajax({
            type: "POST",
            url: '/convert', 
            data: JSON.stringify({
                dpi: parseInt($('#dpi')[0].value),
                jpeg_quality: parseInt($('#jpeg_quality')[0].value),
                target: $('#target')[0].value
            }),
            contentType: 'application/json',
            success: function(data){
                $('#result').html('<div class="alert alert-success">BD convertie</div>')
                console.log(data)
                $('#submit_button').show()
            },
            dataType: 'json'
        })
        .fail((error) => {
            $('#result').html('<div class="alert alert-danger">Erreur dans la convertion.</div>')
            console.log(error)
        })
    })
})

