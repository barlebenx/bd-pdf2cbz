from pdf2image import convert_from_path
import os
import sys
import zipfile
import shutil

def create_images(bd_file, images_dir, dpi=200, jpeg_quality=100):
    # https://pdf2image.readthedocs.io/en/latest/reference.html
    images = convert_from_path(
        bd_file,
        fmt = "jpeg",
        jpegopt={
            "quality": jpeg_quality,
            "progressive": False,
            "optimize": True
        },
        thread_count = 4,
        dpi = dpi
    )
    counter = 1
    nb_page = len(images)
    for image in images:
        print("Build page {}/{}...".format(counter,nb_page))
        image.save(os.path.join(images_dir, 'page_{:03d}.jpg'.format(counter)))
        print("Build page {}/{} done".format(counter,nb_page))
        counter += 1

def create_zip(images_dir, zip_path):
    print("Build zip...")
    zipf = zipfile.ZipFile(zip_path, mode='w', compression=zipfile.ZIP_STORED)

    for dirname, dirs, files in os.walk(images_dir):
        for file in files:
            zipf.write(os.path.join(dirname, file), arcname=file)
    zipf.close()
    print("Build zip done => {}".format(zip_path))
