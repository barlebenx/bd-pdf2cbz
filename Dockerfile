FROM python:3.10-alpine AS deps
RUN pip install --no-cache-dir pipenv==2022.5.2
WORKDIR /
COPY Pipfile .
COPY Pipfile.lock .
RUN apk add --no-cache gcc python3-dev jpeg-dev zlib-dev musl-dev linux-headers
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

FROM python:3.10-alpine
WORKDIR /app
COPY ./app /app
COPY --from=deps /.venv /.venv
RUN apk add --no-cache poppler-utils
ENV PATH="/.venv/bin:$PATH"
ENV PYTHONUNBUFFERED=1
EXPOSE 5000
ENTRYPOINT ["gunicorn", "--bind", "0.0.0.0:5000", "web:app"]
CMD ["--timeout", "0", "--workers", "5"]
